# Ansible Role - Oscap CIS Harden

The following Ansible role, hardens a Rocky 8/9 distribution to the CIS Server
Level 1 standard.

## Context

The following role was used to harden Rocky 8.7 images to CIS Server L1. There
may be 5-6 non-compliant controls that were addressed in subsequent ansible
roles.

## Utilising the role

Use this Ansible role in conjunction with the Oscap\_CIS\_Benchmark to harden
base Linux images.
